TASK TRACKER (ДЕМО)
===================

## Запуск Spring-приложения

Для быстрого запуска приложения и ***postgres*** в докере использовать баш-скрипт: [deployment/tbup.sh](deployment/tbup.sh)

1. Приложение запускается двумя способами: через Run Configuration ***IntelliJ*** и через докер.
2. При запуске через Run Configuration предполагается наличие на локалхосте запущенного postgres-сервера версии _16.2_. Для запуска в докере только контейнера с БД есть compose-файл [deployment/_dev/docker-compose_postgres.yml](deployment/_dev/docker-compose_postgres.yml). Имя пользователя, пароль, название БД, и другие параметры указаны в файле [deployment/_dev/compose.env](deployment/_dev/compose.env). Сам RunConfiguration находится в папке `.run`.
3. Для запуска в докере БД и приложения вместе используется [deployment/_dev/docker-compose.yml](deployment/_dev/docker-compose.yml)
4. Приложение необходимо запускать с переменными окружения из файла [deployment/_dev/compose.env](deployment/_dev/compose.env)). Для ***IntelliJ*** они прописаны в Run Configuration. Для докера прописаны в compose-файле как `compose.env`.

5. Для быстрого запуска приложения и ***postgres*** в докере использовать баш-скрипт: [deployment/tbup.sh](deployment/tbup.sh) (описание скриптов см. ниже) - этот скрипт компилирует проект, билдит докер-образ и запускает два докер-контейнера в отдельной сети.
6. Для быстрого запуска postgres-контейнера использовать [deployment/tup_pg.sh](deployment/tup_pg.sh)

7. Если заняты порты 8080 и 5432, можно изменить значения в `compose.env`

## Краткое описание механики

1. Всем пользователям открыта возможность регистрации и авторизации, остальные возможности закрыты для неавторизованных пользователей
2. Все авторизованные пользователи могут просматривать существующие задачи и создавать новые.
3. Обычные пользователи могут редактировать и удалять только свои задачи.
4. Админ может редактировать и удалять задачи любых пользователей (для упрощения пользователь `admin` создаётся в начальной миграции).
5. При редактировании своей задачи пользователь может назначить другого пользователя ответственным за эту задачу.

## Описание тестов

- 3 unit-теста на три основных сервиса ([TaskService](src/main/java/ru/tasktracker/services/TaskService.java), [JwtService](src/main/java/ru/tasktracker/services/JwtService.java), [UserService](src/main/java/ru/tasktracker/services/UserService.java))
- 1 интеграционный тест с использованием `testcontainers`
- для ручного тестирования запросов - файл [requests.http](requests.http)
- есть Run Configuration для ***Intellij***: [All Tests](.run/All Tests.run.xml)

## Описание основных bash-скриптов

Основные скрипты лежат в папке `deployment`. Предполагается, что файлы `compose.env` не должны коммититься (см. `deployment/.gitignore`), но т.к. задача тестовая для упрощения проверки они закоммичены.

Первая буква ***t*** - скрипты для теста.
Первая буква ***p*** - скрипты для прода.
Предполагается, что разница между тестом и продом - в переменных окружения (`compose.env`).

Тест (для папки `deployment/_dev`):

`tup_pg.sh`   - _test up pg_: поднять postgres-контейнер  
`tdown_pg.sh` - _test down pg_: выключить postgres-контейнер  
`tbuild.sh`   - _test build_: скомпилировать приложение и сбилдить докер-образы  
`tup.sh`      - _test up_: поднять всё приложение в докере  
`tbup.sh`     - _test build up_: скомпилировать и поднять всё приложение в докере  
`tdown.sh`    - _test down_: выключить/удалить контейнеры с приложением и удалить созданную сеть  
`trestart.sh` - _test restart_: перезапуск приложения  

Прод (для папки `deployment/_prod`):

`pup_pg.sh`   - _prod up pg_: поднять postgres-контейнер  
`pdown_pg.sh` - _prod down pg_: выключить postgres-контейнер  
`pbuild.sh`   - _prod build_: скомпилировать приложение и сбилдить докер-образы  
`pup.sh`      - _prod up_: поднять всё приложение в докере  
`pbup.sh`     - _prod build up_: скомпилировать и поднять всё приложение в докере  
`pdown.sh`    - _prod down_: выключить/удалить контейнеры с приложением и удалить созданную сеть  
`prestart.sh` - _prod restart_: перезапуск приложения
