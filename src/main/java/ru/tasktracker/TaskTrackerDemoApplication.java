package ru.tasktracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskTrackerDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskTrackerDemoApplication.class, args);
    }

}
