package ru.tasktracker.services;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.tasktracker.db.entities.User;

import java.util.Optional;

/**
 * Спец-методы, связанные с авторизацией
 */
@Component
public class AuthUtils {

    /**
     * Получить авторизованного пользователя
     * @return Optional, содержащий пользователя, если пользователь авторизован, или пустой, если не авторизован
     */
    public Optional<User> getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        Object principal = authentication.getPrincipal();

        if (principal instanceof User) {
            return Optional.of((User) principal);
        }

        return Optional.empty();
    }

    /**
     * Проверить авторизован ли пользователь с указанным id
     * @param userId - id проверяемого пользователя
     * @return true если авторизован, false если нет
     */
    public boolean isUserAuthenticated(Long userId) {
        return getAuthenticatedUser()
                .map(user -> userId.equals(user.getId()))
                .orElse(false);
    }

}
