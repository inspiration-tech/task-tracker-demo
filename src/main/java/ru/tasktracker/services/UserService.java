package ru.tasktracker.services;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.tasktracker.db.UserRole;
import ru.tasktracker.db.entities.User;
import ru.tasktracker.db.repositories.UserRepository;
import ru.tasktracker.rest.dto.AuthenticationRequestDto;
import ru.tasktracker.rest.dto.AuthenticationResponseDto;
import ru.tasktracker.rest.dto.RegisterRequestDto;

/**
 * Сервис, отвечающий за операции, связанные с пользователем
 */
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * Зарегистрировать нового пользователя
     * @param request - запрос на регистрацию пользователя
     * @return ответ с токеном
     */
    public AuthenticationResponseDto register(RegisterRequestDto request) {
        User user = User.builder()
                .username(request.getUsername())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(UserRole.USER)
                .build();

        userRepository.save(user);

        return AuthenticationResponseDto.builder()
                .token(jwtService.generateToken(user))
                .build();
    }

    /**
     * Авторизовать пользователя (выдать токен)
     * @param request - запрос на авторизацию пользователя
     * @return ответ с токеном
     */
    public AuthenticationResponseDto auth(AuthenticationRequestDto request) {
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                request.getUsername(),
                request.getPassword()
            )
        );

        User user = userRepository
                .findByUsername(request.getUsername())
                .orElseThrow();

        return AuthenticationResponseDto.builder()
                .token(jwtService.generateToken(user))
                .build();
    }

}
