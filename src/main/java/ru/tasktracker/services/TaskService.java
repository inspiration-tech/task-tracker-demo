package ru.tasktracker.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tasktracker.db.UserRole;
import ru.tasktracker.db.entities.Task;
import ru.tasktracker.db.entities.User;
import ru.tasktracker.db.repositories.TaskRepository;
import ru.tasktracker.exceptions.TaskNotFoundException;
import ru.tasktracker.exceptions.TaskOperationException;
import ru.tasktracker.config.mappers.TaskMapper;
import ru.tasktracker.rest.dto.TaskDto;
import ru.tasktracker.rest.dto.TasksPageDto;

/**
 * Сервис, отвечающий за работу с задачами
 */
@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final AuthUtils authUtils;

    /**
     * Получить одну задачу по id
     * @param taskId - id задачи
     * @return dto задачи
     * @throws TaskNotFoundException - если задача не найдена
     */
    public TaskDto getTask(@NonNull Long taskId) throws TaskNotFoundException {
        Task task = taskRepository
                .findById(taskId).orElseThrow(
                        () -> new TaskNotFoundException("Задача не найдена по id: " + taskId)
                );

        return taskMapper.taskToTaskDto(task);
    }

    /**
     * Получить страницу с задачами
     * @param taskLimits - номер страницы и количество элементов на ней
     * @return страницу с задачами
     */
    public Page<TaskDto> getTasks(TasksPageDto taskLimits) {
        PageRequest pageable = PageRequest.of(taskLimits.getPage(), taskLimits.getSize());

        return taskRepository
                .findAll(pageable)
                .map(taskMapper::taskToTaskDto);
    }

    /**
     * Создать задачу
     * @param newTask - dto новой задачи
     * @return dto созданной задачи, включая id
     * @throws TaskOperationException - ошибка при создании задачи
     */
    @Transactional
    public TaskDto createTask(@NonNull TaskDto newTask) throws TaskOperationException {
        User user = authUtils
                .getAuthenticatedUser()
                .orElseThrow(() -> new TaskOperationException("Необходима авторизация"));

        Task task = taskMapper.taskDtoToTask(newTask);
        task.setAuthor(user);
        Task savedTask = taskRepository.save(task);

        return taskMapper.taskToTaskDto(savedTask);
    }

    /**
     * Обновить / отредактировать задачу
     * @param taskId - id задачи
     * @param updatedTask - dto задачи с полями, которые нужно обновить
     * @return dto обновлённой задачи
     * @throws TaskNotFoundException - если задача не найдена
     * @throws TaskOperationException - ошибка при обновлении задачи
     */
    @Transactional
    public TaskDto editTask(@NonNull Long taskId, @NonNull TaskDto updatedTask) throws TaskNotFoundException, TaskOperationException {
        Task existingTask = taskRepository
                .findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException("Задача не найдена по id: " + taskId));

        if (!canUserEditTask(existingTask)) {
            throw new TaskOperationException("Нет прав для редактирования задачи");
        }

        if (updatedTask.getAssigneeId() != null) {
            existingTask.setAssignee(
                User.builder()
                    .id(updatedTask.getAssigneeId())
                    .build()
            );
        }
        if (updatedTask.getName() != null) {
            existingTask.setName(updatedTask.getName());
        }
        if (updatedTask.getDescription() != null) {
            existingTask.setDescription(updatedTask.getDescription());
        }
        if (updatedTask.getStatus() != null) {
            existingTask.setStatus(updatedTask.getStatus());
        }

        Task savedTask = taskRepository.save(existingTask);

        return taskMapper.taskToTaskDto(savedTask);
    }

    /**
     * Удалить задачу
     * @param taskId - id задачи
     * @throws TaskNotFoundException - если задача не найдена
     * @throws TaskOperationException - ошибка при удалении задачи
     */
    @Transactional
    public void deleteTask(@NonNull Long taskId) throws TaskNotFoundException, TaskOperationException {
        Task existingTask = taskRepository
                .findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException("Задача не найдена по id: " + taskId));

        if (!canUserEditTask(existingTask)) {
            throw new TaskOperationException("Нет прав для удаления задачи");
        }

        taskRepository.deleteById(taskId);
    }

    /**
     * Проверить, есть ли у текущего (авторизованного) пользователя права на редактирование/удаление задачи (т.е. либо имеет роль админа, либо автор задачи)
     * @param task - задача, для которой делается проверка
     * @return true если есть права, false если нет
     */
    private boolean canUserEditTask(@NonNull Task task) {
        if (task.getAuthor() == null) {
            throw new IllegalArgumentException("Невалидный экземпляр задачи: отсутствует автор");
        }

        return authUtils.getAuthenticatedUser()
                .map(user -> task.getAuthor().getId().equals(user.getId()) ||
                        user.getRole().equals(UserRole.ADMIN))
                .orElse(false);
    }

}
