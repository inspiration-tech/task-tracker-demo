package ru.tasktracker.services;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.*;
import java.util.function.Function;

/**
 * Сервис, отвечающий за работу с jwt-токенами
 */
@Service
public class JwtService {

    @Value("${jwt.secret-key}")
    private String secretKey;

    /**
     * Извлечь имя пользователя из токена
     * @param jwt - токен
     * @return имя пользователя
     */
    public String extractUsername(String jwt) {
        return extractClaim(jwt, Claims::getSubject);
    }

    /**
     * Извлечь дату окончания срока действия
     * @param jwt - токен
     * @return дата окончания срока действия
     */
    public Date extractExpiration(String jwt) {
        return extractClaim(jwt, Claims::getExpiration);
    }

    /**
     * Извлечь "клейм" из токена
     * @param jwt - токен
     * @param claimsResolver - функция, извлекающая клейм
     * @return - извлечённый клейм
     * @param <T> - тип извлекаемого клейма
     */
    public <T> T extractClaim(String jwt, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(jwt);

        return claimsResolver.apply(claims);
    }

    /**
     * Сгенерировать токен
     * @param userDetails - объект UserDetails
     * @return сгенерированный токен
     */
    public String generateToken(
            UserDetails userDetails
    ) {
        return generateToken(userDetails, new HashMap<>());
    }

    /**
     * Сгенерировать токен
     * @param userDetails - объект UserDetails
     * @param extraClaims - дополнительные клеймы
     * @return сгенерированный токен
     */
    public String generateToken(
            UserDetails userDetails,
            Map<String, Object> extraClaims
    ) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 1);
        Date expirationDate = calendar.getTime();

        return Jwts.builder()
                .setClaims(extraClaims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(expirationDate)
                .signWith(getSigningKey(), SignatureAlgorithm.HS256)
                .compact();
    }

    /**
     * Проверить валидность токена
     * @param jwt - токен
     * @param userDetails - объект UserDetails
     * @return true - валидный, false - невалидный
     */
    public boolean isTokenValid(String jwt, UserDetails userDetails) {
        if (isTokenExpired(jwt)) {
            return false;
        }

        return extractUsername(jwt).equals(userDetails.getUsername());
    }

    /**
     * Проверить, истёк срок действия токена или нет
     * @param jwt - токен
     * @return true - истёк, false - не истёк
     */
    private boolean isTokenExpired(String jwt) {
        try {
            return extractExpiration(jwt).before(new Date());
        } catch (ExpiredJwtException e) {
            return true;
        }
    }

    /**
     * Извлечь все "клеймы" из токена
     * @param jwt - токен
     * @return объект Claims
     */
    private Claims extractAllClaims(String jwt) {
        return Jwts.parserBuilder()
                .setSigningKey(getSigningKey())
                .build()
                .parseClaimsJws(jwt)
                .getBody();
    }

    /**
     * Получить ключ подписи для секретного ключа
     * @return ключ подписи
     */
    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);

        return Keys.hmacShaKeyFor(keyBytes);
    }

}
