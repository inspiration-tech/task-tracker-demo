package ru.tasktracker.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tasktracker.rest.dto.AuthenticationRequestDto;
import ru.tasktracker.rest.dto.AuthenticationResponseDto;
import ru.tasktracker.rest.dto.RegisterRequestDto;
import ru.tasktracker.services.UserService;

@RestController
@RequestMapping(path = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {

    private final UserService userService;

    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponseDto> register(
            @RequestBody RegisterRequestDto request
    ) {
        return ResponseEntity.ok(userService.register(request));
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationResponseDto> auth(
            @RequestBody AuthenticationRequestDto request
    ) {
        return ResponseEntity.ok(userService.auth(request));
    }

}
