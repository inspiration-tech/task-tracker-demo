package ru.tasktracker.rest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.tasktracker.exceptions.TaskNotFoundException;
import ru.tasktracker.exceptions.TaskOperationException;
import ru.tasktracker.rest.dto.TaskDto;
import ru.tasktracker.rest.dto.TasksPageDto;
import ru.tasktracker.services.TaskService;

import javax.validation.Valid;
import java.net.URI;

@Validated
@RestController
@RequestMapping(path = "/api/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public ResponseEntity<Page<TaskDto>> getAllTasks(
            @Valid @RequestBody TasksPageDto taskLimits) {
        return ResponseEntity.ok(taskService.getTasks(taskLimits));
    }

    @GetMapping("/{taskId}")
    public ResponseEntity<TaskDto> getTask(@PathVariable Long taskId) throws TaskNotFoundException {
        return ResponseEntity.ok(taskService.getTask(taskId));
    }

    @PostMapping
    public ResponseEntity<TaskDto> createTask(@Valid @RequestBody TaskDto task) throws TaskOperationException {
        TaskDto createdTask = taskService.createTask(task);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(createdTask.getId())
                .toUri();

        return ResponseEntity
                .created(location)
                .body(createdTask);
    }

    @PatchMapping("/{taskId}")
    public ResponseEntity<TaskDto> editTask(@PathVariable Long taskId, @RequestBody TaskDto task) throws TaskNotFoundException, TaskOperationException {
        TaskDto editedTaskDto = taskService.editTask(taskId, task);

        return ResponseEntity.status(HttpStatus.OK).body(editedTaskDto);
    }

    @DeleteMapping("/{taskId}")
    public ResponseEntity<Void> deleteTask(@PathVariable Long taskId) throws TaskNotFoundException, TaskOperationException {
        taskService.deleteTask(taskId);

        return ResponseEntity.noContent().build();
    }

}
