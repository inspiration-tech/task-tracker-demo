package ru.tasktracker.rest;

import javax.servlet.http.HttpServletRequest;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.tasktracker.exceptions.TaskException;
import ru.tasktracker.rest.dto.ErrorResponseDto;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice(basePackages = {"ru.tasktracker"})
class GlobalRestExceptionHandler {

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<ErrorResponseDto> handleException(ExpiredJwtException e) {
        String errorText = "Необоходима повторная авторизация";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ErrorResponseDto(errorText));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ErrorResponseDto> handleException(BadCredentialsException e) {
        String errorText = "Некорректный логин или пароль";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ErrorResponseDto(errorText));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        Map<String, String> errors = new HashMap<>();

        for (FieldError fieldError : result.getFieldErrors()) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }

        return ResponseEntity.badRequest().body(errors);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponseDto> handleException(MissingServletRequestParameterException e) {
        String errorText = "Отсутствует обязательный параметр \""+e.getParameterName()+"\"";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .badRequest()
                .body(new ErrorResponseDto(errorText));
    }

    @ExceptionHandler({
            IllegalArgumentException.class,
            HttpMessageNotReadableException.class
    })
    public ResponseEntity<ErrorResponseDto> handleException(IllegalArgumentException e) {
        String errorText = "Некорректные параметры запроса";

        log.info(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .badRequest()
                .body(new ErrorResponseDto(errorText));
    }

    @ExceptionHandler(TaskException.class)
    public ResponseEntity<ErrorResponseDto> handleException(TaskException e) {
        String errorText = e.getMessage();

        log.info(e.getClass().getName()+": "+errorText);
        log.debug(errorText, e);

        return ResponseEntity
                .status(getExceptionStatus(e))
                .body(new ErrorResponseDto(errorText));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponseDto> handleAnyException(Exception e) {

        String errorText = "На сервере произошла ошибка. Пожалуйста, повторите попытку позднее.";

        log.error(e.getClass().getName()+": "+e.getMessage());
        log.debug(e.getMessage(), e);

        return ResponseEntity
                .status(getExceptionStatus(e))
                .body(new ErrorResponseDto(errorText));
    }

    private HttpStatus getExceptionStatus(Exception exception) {
        ResponseStatus responseStatus = exception.getClass().getAnnotation(ResponseStatus.class);

        if (responseStatus != null) {
            return responseStatus.value();
        }

        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
