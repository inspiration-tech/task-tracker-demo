package ru.tasktracker.rest.dto;

import lombok.Getter;

import javax.validation.constraints.Min;

/**
 * Представляет собой страницу с задачами и количество элементов на ней
 */
@Getter
public final class TasksPageDto {

    @Min(value = 0, message = "Страница не может быть меньше 0")
    private final int page;

    private final int size;

    public TasksPageDto() {
        this.page = 0;
        this.size = 10;
    }

}
