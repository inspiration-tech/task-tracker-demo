package ru.tasktracker.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Представляет собой http-ответ на методы связанные с регистрацией и авторизацией
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public final class AuthenticationResponseDto {

    private String token;

}
