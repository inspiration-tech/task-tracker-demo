package ru.tasktracker.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.tasktracker.db.TaskStatus;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Представляет собой задачу
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public final class TaskDto {

    private final static String REQUIRED_PARAM_MESSAGE = "Параметр является обязательным";

    private Long id;

    @NotNull(message = REQUIRED_PARAM_MESSAGE)
    private String name;

    private String description;

    private TaskStatus status;

    private Long authorId;

    private Long assigneeId;

    private Date createdAt;

    private Date updatedAt;

}
