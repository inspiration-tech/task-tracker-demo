package ru.tasktracker.rest.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Представляет собой запрос на регистрацию нового пользователя
 */
@Data
@Builder
public final class RegisterRequestDto {

    private String username;
    private String email;
    private String password;

}
