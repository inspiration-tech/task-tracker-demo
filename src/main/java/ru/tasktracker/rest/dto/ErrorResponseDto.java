package ru.tasktracker.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Представляет собой http-ответ с ошибкой
 */
@Data
@AllArgsConstructor
public final class ErrorResponseDto {

    private String error;

}
