package ru.tasktracker.rest.dto;

import lombok.Builder;
import lombok.Data;

/**
 * Представляет собой запрос на авторизацию
 */
@Data
@Builder
public final class AuthenticationRequestDto {

    private String username;
    private String password;

}
