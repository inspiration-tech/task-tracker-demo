package ru.tasktracker.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Класс представляет собой исключение, которое кидается если задача не найдена
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class TaskNotFoundException extends TaskException {

    /**
     * Конструктор исключения с указанием сообщения об ошибке
     *
     * @param message - сообщение об ошибке
     */
    public TaskNotFoundException(String message) {
        super(message);
    }

    /**
     * Конструктор исключения с указанием сообщения об ошибке и причины исключения
     *
     * @param message - сообщение об ошибке
     * @param throwable - причина исключения
     */
    public TaskNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
