package ru.tasktracker.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Класс представляет собой исключение, которое кидается при ошибках в работе с задачей
 */
@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class TaskException extends Exception {

    /**
     * Конструктор исключения с указанием сообщения об ошибке
     *
     * @param message - сообщение об ошибке
     */
    public TaskException(String message) {
        super(message);
    }

    /**
     * Конструктор исключения с указанием сообщения об ошибке и причины исключения
     *
     * @param message - сообщение об ошибке
     * @param throwable - причина исключения
     */
    public TaskException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
