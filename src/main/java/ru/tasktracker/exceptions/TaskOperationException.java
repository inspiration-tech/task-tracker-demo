package ru.tasktracker.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Класс представляет собой исключение, которое кидается при ошибках при операциях с задачей
 */
@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class TaskOperationException extends TaskException {

    /**
     * Конструктор исключения с указанием сообщения об ошибке
     *
     * @param message - сообщение об ошибке
     */
    public TaskOperationException(String message) {
        super(message);
    }

    /**
     * Конструктор исключения с указанием сообщения об ошибке и причины исключения
     *
     * @param message - сообщение об ошибке
     * @param throwable - причина исключения
     */
    public TaskOperationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
