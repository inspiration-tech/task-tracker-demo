package ru.tasktracker.db.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.tasktracker.db.entities.User;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsername(String username);

}
