package ru.tasktracker.db.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.tasktracker.db.entities.Task;

public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {
}
