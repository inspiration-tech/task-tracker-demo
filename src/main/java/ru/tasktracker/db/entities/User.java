package ru.tasktracker.db.entities;

import lombok.*;

import lombok.experimental.SuperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.tasktracker.db.UserRole;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

/**
 * Сущность, представляющая собой пользователя
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "users")
public class User extends AbstractEntity implements UserDetails {

    /**
     * Задачи, созданные пользователем
     */
    @OneToMany(mappedBy = "author")
    @ToString.Exclude
    protected List<Task> createdTasks;

    /**
     * Задачи, назначенные пользователю
     */
    @OneToMany(mappedBy = "assignee")
    @ToString.Exclude
    protected List<Task> assignedTasks;

    /**
     * Имя пользователя / логин
     */
    @Column(name = "username", nullable = false, unique = true)
    protected String username;

    /**
     * Email пользователя
     */
    @Column(name = "email", nullable = false, unique = true)
    protected String email;

    /**
     * Пароль пользователя
     */
    @Column(name = "password", nullable = false)
    protected String password;

    /**
     * Роль пользователя в системе
     */
    @Column(name = "role", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    protected UserRole role = UserRole.USER;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
