package ru.tasktracker.db.entities;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.tasktracker.db.TaskStatus;

import javax.persistence.*;

/**
 * Сущность, представляющая собой задачу
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(
    name = "tasks",
    indexes = {
        @Index(columnList = "author_id"),
        @Index(columnList = "assignee_id")
    }
)
public class Task extends AbstractEntity {

    /**
     * Автор задачи
     */
    @ManyToOne
    @JoinColumn(name = "author_id", nullable = false)
    protected User author;

    /**
     * Ответственное лицо за выполнение задачи
     */
    @ManyToOne
    @JoinColumn(name = "assignee_id")
    protected User assignee;

    /**
     * Название задачи
     */
    @Column(nullable = false)
    protected String name;

    /**
     * Описание задачи
     */
    @Column
    protected String description;

    /**
     * Статус задачи
     */
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    protected TaskStatus status = TaskStatus.NEW;

}
