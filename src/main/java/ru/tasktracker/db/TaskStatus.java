package ru.tasktracker.db;

/**
 * Статус задачи
 */
public enum TaskStatus {
    NEW, // новая
    ACTIVE, // в работе
    COMPLETED // завершена
}
