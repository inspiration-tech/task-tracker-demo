package ru.tasktracker.config.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Логирование сервисов
 */
@Slf4j
@Aspect
@Component
public class ServiceLoggingAspect {

    @Pointcut("execution(* ru.tasktracker.services..*.*(..))")
    public void allMethods() {}

    @Before("allMethods()")
    public void beforeUpdateUserBalance(JoinPoint joinPoint) {
        String argsString = Arrays
                .stream(joinPoint.getArgs())
                .map(Object::toString)
                .collect(Collectors.joining(" / "));

        log.info("Before service method "+joinPoint.getSignature().toShortString()+". Args: "+argsString);
    }

    @AfterReturning(pointcut = "allMethods()", returning = "result")
    public void afterUpdateUserBalance(JoinPoint joinPoint, Object result) {
        String methodSignature = joinPoint.getSignature().toShortString();
        log.info("After {} method. Returned Value: {}", methodSignature, result);
    }

    @AfterThrowing(pointcut = "allMethods()", throwing = "exception")
    public void afterUpdateUserBalance(JoinPoint joinPoint, Throwable exception) {
        String methodSignature = joinPoint.getSignature().toShortString();
        log.info("Exception after {} method. Exception: {}", methodSignature, exception.getMessage());
    }

}
