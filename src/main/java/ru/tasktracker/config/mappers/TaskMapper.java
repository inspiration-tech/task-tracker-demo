package ru.tasktracker.config.mappers;

import org.mapstruct.*;
import ru.tasktracker.db.TaskStatus;
import ru.tasktracker.db.entities.Task;
import ru.tasktracker.db.entities.User;
import ru.tasktracker.rest.dto.TaskDto;

@Mapper
public interface TaskMapper {

    @Mapping(source = "author.id", target = "authorId")
    @Mapping(source = "assignee.id", target = "assigneeId")
    TaskDto taskToTaskDto(Task task);

    @Mapping(source = "authorId", target = "author", qualifiedByName = "userIdToUser")
    @Mapping(source = "assigneeId", target = "assignee", qualifiedByName = "userIdToUser")
    @Mapping(source = "status", target = "status", qualifiedByName = "statusMapping")
    Task taskDtoToTask(TaskDto taskDto);

    @Named("userIdToUser")
    default User mapUserIdToUser(Long userId) {
        return userId != null ? User.builder().id(userId).build() : null;
    }

    @Named("statusMapping")
    default TaskStatus mapStatus(TaskStatus taskStatus) {
        return taskStatus != null ? taskStatus : TaskStatus.NEW;
    }

}
