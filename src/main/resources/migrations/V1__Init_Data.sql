-- СОЗДАНИЕ ТАБЛИЦ --

CREATE TABLE users (
    id         BIGSERIAL    PRIMARY KEY,
    username   VARCHAR(255) NOT NULL
                CONSTRAINT uk_r43af9ap4edm43mmtq01oddj6
                    UNIQUE,
    email      VARCHAR(255) NOT NULL
                CONSTRAINT uk_6dotkott2kjsp8vw4d0m25fb7
                    UNIQUE,
    password   VARCHAR(255) NOT NULL,
    role       VARCHAR(20)  NOT NULL DEFAULT 'USER',
    created_at TIMESTAMP    NOT NULL DEFAULT current_timestamp,
    updated_at TIMESTAMP
);

CREATE TABLE tasks (
    id          BIGSERIAL   PRIMARY KEY,
    author_id   BIGINT      NOT NULL
                 CONSTRAINT fkhods8r8oyyx7tuj3c91ki2sk1
                     REFERENCES users,
    assignee_id BIGINT
                 CONSTRAINT fkekr1dgiqktpyoip3qmp6lxsit
                     REFERENCES users,
    name        VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    status      VARCHAR(255) NOT NULL DEFAULT 'NEW',
    created_at  TIMESTAMP    NOT NULL DEFAULT current_timestamp,
    updated_at  TIMESTAMP
);

-- СОЗДАНИЕ ИНДЕКСОВ --

CREATE INDEX idxk921h22dci6tov4o6xnpamdb8
    ON tasks (author_id);

CREATE INDEX idx29mywj10dui941nqk773wvuxj
    ON tasks (assignee_id);

-- ЗАПОЛНЕНИЕ ТЕСТОВЫМИ ДАННЫМИ --

INSERT INTO users
    (id, username, email, password, role)
VALUES
    (1, 'user1', 'user1@example.com', '$2a$10$fBCwuPsz5vHsun9MF1ICv.xWNZwqHfRElZ4axGldjHQjkiPQoFavy', 'USER'),
    (2, 'user2', 'user2@example.com', '$2a$10$fBCwuPsz5vHsun9MF1ICv.xWNZwqHfRElZ4axGldjHQjkiPQoFavy', 'USER'),
    (3, 'admin', 'admin@example.com', '$2a$10$fBCwuPsz5vHsun9MF1ICv.xWNZwqHfRElZ4axGldjHQjkiPQoFavy', 'ADMIN');

INSERT INTO tasks
    (author_id, assignee_id, name, description, status)
VALUES
    (2, null, 'Задача 1', null, 'NEW'),
    (2, null, 'Задача 2', null, 'NEW'),
    (2, 1, 'Задача 3', null, 'ACTIVE'),
    (2, 1, 'Задача 4', null, 'ACTIVE'),
    (2, 1, 'Задача 5', null, 'COMPLETED'),
    (2, 1, 'Задача 6', 'Задача с описанием', 'COMPLETED'),
    (2, 2, 'Задача 7', null, 'COMPLETED'),
    (2, 2, 'Задача 8', null, 'COMPLETED'),
    (3, null, 'Задача 9', null, 'NEW'),
    (3, null, 'Задача 10', null, 'NEW'),
    (3, null, 'Задача 11', null, 'NEW'),
    (3, null, 'Задача 12', null, 'NEW');

ALTER SEQUENCE users_id_seq RESTART WITH 4;
