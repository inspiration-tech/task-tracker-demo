package ru.tasktracker;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.TestExecutionEvent;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.tasktracker.db.TaskStatus;
import ru.tasktracker.rest.dto.AuthenticationRequestDto;
import ru.tasktracker.rest.dto.AuthenticationResponseDto;
import ru.tasktracker.rest.dto.TaskDto;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
@AutoConfigureMockMvc
@Transactional
@SpringBootTest(properties = {
    "spring.config.location=classpath:test.properties"
})
public class TaskTrackerDemoApplicationTests {

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private MockMvc mockMvc;

    private static String migrationInitSql;

    @BeforeAll
    private static void init() throws IOException {
        migrationInitSql = Files.readString(
                Paths.get("src/main/resources/migrations/V1__Init_Data.sql")
        );
    }

    @BeforeEach
    private void initData() {
        jdbcTemplate.execute(migrationInitSql);
    }

    /**
     * Авторизация с неверным логином/паролем
     */
    @Test
    public void testLoginBadCredentials() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AuthenticationRequestDto authRequest = AuthenticationRequestDto.builder()
                .username("admin")
                .password("12345") // неверный пароль
                .build();
        final String requestJson = objectMapper.writeValueAsString(authRequest);

        mockMvc.perform(
                MockMvcRequestBuilders
                    .post("/auth/authenticate")
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    /**
     * Успешная авторизация с возможностью последующего просмотра задач
     */
    @Test
    public void testLoginSuccess() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        AuthenticationRequestDto authRequest = AuthenticationRequestDto.builder()
                .username("user1")
                .password("123456") // верный пароль
                .build();
        final String requestJson = objectMapper.writeValueAsString(authRequest);

        MockHttpServletResponse response = mockMvc.perform(
                    MockMvcRequestBuilders
                        .post("/auth/authenticate")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        String responseContent = response.getContentAsString();
        AuthenticationResponseDto authResponse = objectMapper.readValue(responseContent, AuthenticationResponseDto.class);

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get("/api/tasks/1")
                                .header(HttpHeaders.AUTHORIZATION, "Bearer " + authResponse.getToken())
                )
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Запрещено создавать задачи неавторизованным
     */
    @Test
    public void testCreateUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/tasks"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Запрещено просматривать задачи неавторизованным
     */
    @Test
    public void testGetTasksUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tasks"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Запрещено просматривать конкретную задачу неавторизованным
     */
    @Test
    public void testGetTaskUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tasks/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Запрещено редактировать задачи неавторизованным
     */
    @Test
    public void testEditUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/api/tasks/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Запрещено удалять задачи неавторизованным
     */
    @Test
    public void testDeleteUnauthorized() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/tasks/1"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Тест, что если авторизован, может просматривать список задач, задачи корректно выводятся
     */
    @Test
    @WithMockUser(username = "user1")
    public void testGetTasksAsAuthorizedUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode requestNode = objectMapper.createObjectNode();
        requestNode.put("page", 0);
        requestNode.put("size", 5);

        MockHttpServletResponse response = mockMvc.perform(
                    MockMvcRequestBuilders
                        .get("/api/tasks")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(requestNode.toString())
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        JsonNode responseNode = objectMapper.readTree(response.getContentAsString(StandardCharsets.UTF_8));

        assertEquals(12, responseNode.get("totalElements").asInt());
        assertEquals(5, responseNode.get("content").size());
        assertEquals(3, responseNode.get("totalPages").asInt());
        assertTrue(responseNode.get("first").asBoolean());
    }

    /**
     * Тест, что если авторизован, может просматривать конкретную задачу
     */
    @Test
    @WithMockUser(username = "user1")
    public void testGetTaskAsAuthorizedUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        MockHttpServletResponse response = mockMvc.perform(
                        MockMvcRequestBuilders
                                .get("/api/tasks/6")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        TaskDto task = objectMapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), TaskDto.class);

        assertEquals("Задача 6", task.getName());
        assertEquals("Задача с описанием", task.getDescription());
        assertEquals(1, task.getAssigneeId());
        assertEquals(TaskStatus.COMPLETED, task.getStatus());
    }

    /**
     * Тест, что простому пользователю нельзя редактировать чужие задачи
     */
    @Test
    @WithMockUser(username = "user1")
    public void testEditNonOwnedTask() throws Exception {
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .patch("/api/tasks/6")
                                .content("{}")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
    }

    /**
     * Тест, что простому пользователю нельзя удалять чужие задачи
     */
    @Test
    @WithMockUser(username = "user1")
    public void testDeleteNonOwnedTask() throws Exception {
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete("/api/tasks/6")
                                .content("{}")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());
    }

    /**
     * Тест, что простому пользователю можно создавать задачи
     */
    @Test
    @WithUserDetails(
        value = "user1",
        userDetailsServiceBeanName = "userDetailsService",
        setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    public void testCreateAsAuthorizedUser() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        TaskDto createdTask = TaskDto.builder()
                .name("Новая задача")
                .build();
        String requestJson = objectMapper.writeValueAsString(createdTask);

        MockHttpServletResponse response = mockMvc.perform(
                        MockMvcRequestBuilders
                                .post("/api/tasks")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                .content(requestJson)
                )
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andReturn().getResponse();

        TaskDto task = objectMapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), TaskDto.class);

        assertNotNull(task.getId());
        assertEquals("Новая задача", task.getName());
        assertNull(task.getDescription());
        assertEquals(1, task.getAuthorId());
        assertNull(task.getAssigneeId());
        assertEquals(TaskStatus.NEW, task.getStatus());
    }

    /**
     * Тест, что можно редактировать свои задачи
     */
    @Test
    @WithUserDetails(
        value = "user2",
        userDetailsServiceBeanName = "userDetailsService",
        setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    public void testEditAsAuthor() throws Exception {
        editTaskTest();
    }

    /**
     * Тест, что админ может редактировать чужие задачи
     */
    @Test
    @WithUserDetails(
            value = "admin",
            userDetailsServiceBeanName = "userDetailsService",
            setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    public void testEditAsAdmin() throws Exception {
        editTaskTest();
    }

    /**
     * Тест, что можно удалять свои задачи
     */
    @Test
    @WithUserDetails(
        value = "user2",
        userDetailsServiceBeanName = "userDetailsService",
        setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    public void testDeleteAsAuthor() throws Exception {
        deleteTaskTest();
    }

    /**
     * Тест, что админ может удалять чужие задачи
     */
    @Test
    @WithUserDetails(
        value = "admin",
        userDetailsServiceBeanName = "userDetailsService",
        setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    public void testDeleteAsAdmin() throws Exception {
        deleteTaskTest();
    }

    // PRIVATES

    private void deleteTaskTest() throws Exception {
        mockMvc.perform(
                        MockMvcRequestBuilders
                                .delete("/api/tasks/6")
                                .content("{}")
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        mockMvc.perform(
                        MockMvcRequestBuilders
                                .get("/api/tasks/6")
                )
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    private void editTaskTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        TaskDto createdTask = TaskDto.builder()
                .name("Переименование")
                .assigneeId(2L)
                .build();
        String requestJson = objectMapper.writeValueAsString(createdTask);

        MockHttpServletResponse response = mockMvc.perform(
                        MockMvcRequestBuilders
                                .patch("/api/tasks/6")
                                .content(requestJson)
                                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn().getResponse();

        TaskDto task = objectMapper.readValue(response.getContentAsString(StandardCharsets.UTF_8), TaskDto.class);

        assertEquals(6L, task.getId());
        assertEquals("Переименование", task.getName());
        assertEquals("Задача с описанием", task.getDescription());
        assertEquals(2L, task.getAuthorId());
        assertEquals(2L, task.getAssigneeId());
        assertEquals(TaskStatus.COMPLETED, task.getStatus());
    }

}
