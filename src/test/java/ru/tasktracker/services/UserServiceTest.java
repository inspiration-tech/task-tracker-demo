package ru.tasktracker.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.tasktracker.db.UserRole;
import ru.tasktracker.db.entities.User;
import ru.tasktracker.db.repositories.UserRepository;
import ru.tasktracker.rest.dto.AuthenticationRequestDto;
import ru.tasktracker.rest.dto.AuthenticationResponseDto;
import ru.tasktracker.rest.dto.RegisterRequestDto;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtService jwtService;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserService userService;

    private AutoCloseable openMocks;

    @BeforeEach
    public void setUp() {
        openMocks = openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        openMocks.close();
    }

    /**
     * Успешная регистрация
     */
    @Test
    void testRegisterSuccess() {
        String testEncryptedPassword = "encodedPassword";
        String testGeneratedToken = "generatedToken";

        RegisterRequestDto registerRequestDto = RegisterRequestDto.builder()
                .username("new_user")
                .email("user@example.com")
                .password("1234567")
                .build();

        when(passwordEncoder.encode(registerRequestDto.getPassword())).thenReturn(testEncryptedPassword);
        when(jwtService.generateToken(any(User.class))).thenReturn(testGeneratedToken);

        AuthenticationResponseDto response = userService.register(registerRequestDto);

        assertEquals(testGeneratedToken, response.getToken());
        verify(passwordEncoder).encode(registerRequestDto.getPassword());
        verify(userRepository).save(any(User.class));
        verify(jwtService).generateToken(any(User.class));
    }

    /**
     * Успешная авторизация
     */
    @Test
    void testAuthSuccess() {
        String testGeneratedToken = "generatedToken";

        AuthenticationRequestDto authenticationRequestDto = AuthenticationRequestDto.builder()
                .username("new_user")
                .password("1234567")
                .build();

        User user = User.builder()
                .username(authenticationRequestDto.getUsername())
                .email("user@example.com")
                .password("encodedPassword")
                .role(UserRole.USER)
                .build();

        when(userRepository.findByUsername(authenticationRequestDto.getUsername())).thenReturn(Optional.of(user));
        when(jwtService.generateToken(user)).thenReturn(testGeneratedToken);

        AuthenticationResponseDto response = userService.auth(authenticationRequestDto);

        assertEquals(testGeneratedToken, response.getToken());
        verify(authenticationManager).authenticate(any(UsernamePasswordAuthenticationToken.class));
        verify(userRepository).findByUsername(authenticationRequestDto.getUsername());
        verify(jwtService).generateToken(user);
    }

    /**
     * Неуспешная авторизация
     */
    @Test
    void testAuthBadCredentials() {
        AuthenticationRequestDto authenticationRequestDto = AuthenticationRequestDto.builder()
                .username("new_user")
                .password("1234567")
                .build();

        when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
                .thenThrow(new BadCredentialsException("Неверный логин или пароль"));

        assertThrows(BadCredentialsException.class, () -> userService.auth(authenticationRequestDto));

        verify(authenticationManager).authenticate(
                new UsernamePasswordAuthenticationToken(authenticationRequestDto.getUsername(), authenticationRequestDto.getPassword())
        );
        verifyNoInteractions(userRepository);
        verifyNoInteractions(jwtService);
    }

}
