package ru.tasktracker.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import ru.tasktracker.config.mappers.TaskMapper;
import ru.tasktracker.db.UserRole;
import ru.tasktracker.db.entities.Task;
import ru.tasktracker.db.entities.User;
import ru.tasktracker.db.repositories.TaskRepository;
import ru.tasktracker.exceptions.TaskNotFoundException;
import ru.tasktracker.exceptions.TaskOperationException;
import ru.tasktracker.rest.dto.TaskDto;
import ru.tasktracker.rest.dto.TasksPageDto;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class TaskServiceTest {

    @Mock
    private User userMock;

    @Mock
    private TaskRepository taskRepositoryMock;

    @Mock
    private TaskMapper taskMapperMock;

    @Mock
    private AuthUtils authUtils;

    @InjectMocks
    private TaskService taskService;

    private AutoCloseable openMocks;

    @BeforeEach
    public void setUp() {
        openMocks = openMocks(this);
        when(userMock.getId()).thenReturn(1L);
    }

    @AfterEach
    void closeService() throws Exception {
        openMocks.close();
    }

    /**
     * Получение существующей задачи
     */
    @Test
    public void testGetTaskExisting() throws TaskNotFoundException {
        Long taskId = 1L;
        Task task = Task.builder().id(taskId).build();
        TaskDto taskDto = TaskDto.builder().id(taskId).build();

        when(taskRepositoryMock.findById(taskId)).thenReturn(Optional.of(task));
        when(taskMapperMock.taskToTaskDto(task)).thenReturn(taskDto);

        TaskDto result = taskService.getTask(taskId);

        assertNotNull(result);
        assertEquals(taskId, result.getId());
    }

    /**
     * Получение несуществующей задачи
     */
    @Test
    public void testGetTaskNonExisting() {
        Long taskId = 1L;

        when(taskRepositoryMock.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(TaskNotFoundException.class, () -> taskService.getTask(taskId));
    }

    /**
     * Получение списка задач
     */
    @Test
    public void testGetTasks() {
        TasksPageDto taskLimits = new TasksPageDto();
        PageRequest pageable = PageRequest.of(taskLimits.getPage(), taskLimits.getSize());
        Long taskId = 1L;
        Task task = Task.builder().id(taskId).build();
        TaskDto taskDto = TaskDto.builder().id(taskId).build();

        when(taskRepositoryMock.findAll(pageable)).thenReturn(new PageImpl<>(Collections.nCopies(10, task), pageable, 15));
        when(taskMapperMock.taskToTaskDto(task)).thenReturn(taskDto);

        Page<TaskDto> result = taskService.getTasks(taskLimits);

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(10, result.getContent().size());
        assertEquals(2, result.getTotalPages());
        assertEquals(15, result.getTotalElements());
        assertEquals(taskDto.getId(), result.getContent().get(0).getId());
    }

    /**
     * Тест создания задачи
     */
    @Test
    public void testCreateTask() throws TaskOperationException {
        TaskDto newTaskDto = TaskDto.builder()
                .name("Задача 1")
                .build();
        User user = User.builder()
                .id(1L)
                .role(UserRole.USER)
                .build();
        Task task = Task.builder()
                .id(1L)
                .build();

        when(taskMapperMock.taskDtoToTask(newTaskDto)).thenReturn(task);
        when(taskRepositoryMock.save(task)).thenReturn(task);
        when(taskMapperMock.taskToTaskDto(task)).thenReturn(newTaskDto);
        when(authUtils.getAuthenticatedUser()).thenReturn(Optional.of(user));

        TaskDto result = taskService.createTask(newTaskDto);

        assertNotNull(result);
        assertEquals(newTaskDto.getName(), result.getName());
    }

    /**
     * Тест редактирования задачи
     */
    @Test
    public void testEditTask() throws TaskNotFoundException, TaskOperationException {
        Long taskId = 1L;
        TaskDto updatedTaskDto = TaskDto.builder()
                .name("Задача 1")
                .description("Описание к задаче")
                .build();

        User user = User.builder()
                .id(1L)
                .build();

        Task existingTask = Task.builder()
                .id(taskId)
                .author(user)
                .build();

        when(taskRepositoryMock.findById(taskId)).thenReturn(Optional.of(existingTask));
        when(taskRepositoryMock.save(existingTask)).thenReturn(existingTask);
        when(taskMapperMock.taskToTaskDto(existingTask)).thenReturn(updatedTaskDto);

        assertThrows(TaskOperationException.class, () -> taskService.editTask(taskId, updatedTaskDto));

        when(authUtils.getAuthenticatedUser()).thenReturn(Optional.of(user));

        TaskDto result = taskService.editTask(taskId, updatedTaskDto);

        assertNotNull(result);
        assertEquals(updatedTaskDto.getName(), result.getName());
        assertEquals(updatedTaskDto.getDescription(), result.getDescription());

        verify(taskRepositoryMock, times(1)).save(existingTask);
    }

    /**
     * Тест удаления существующей задачи
     */
    @Test
    public void testDeleteTaskExisting() throws TaskNotFoundException, TaskOperationException {
        Long taskId = 1L;
        Task invalidTask = Task.builder()
                .id(taskId)
                .build();

        User user = User.builder()
                .id(2L)
                .build();

        Task validTask = Task.builder()
                .id(taskId)
                .author(user)
                .build();

        when(taskRepositoryMock.findById(taskId))
                .thenReturn(Optional.of(invalidTask))
                .thenReturn(Optional.of(validTask));

        assertThrows(IllegalArgumentException.class, () -> taskService.deleteTask(taskId));

        when(authUtils.getAuthenticatedUser())
                .thenReturn(Optional.of(user));

        taskService.deleteTask(taskId);

        verify(taskRepositoryMock, times(1)).deleteById(taskId);
    }

    /**
     * Тест удаления несуществующей задачи
     */
    @Test
    public void testDeleteTaskNonExisting() {
        Long taskId = 1L;

        when(taskRepositoryMock.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(TaskNotFoundException.class, () -> taskService.deleteTask(taskId));
    }

}
