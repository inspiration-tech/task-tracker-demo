#!/bin/bash

WORKING_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" # script directory

# Check that at least one param is present
if [ $# -lt 1 ]; then
    echo "Usage: $0 <dev|prod> [optional_arg1] [optional_arg2] ..."
    exit 1
fi

# Check if the parameter is either "dev" or "prod"
if [ "$1" != "dev" ] && [ "$1" != "prod" ]; then
    echo "Invalid parameter. Please specify 'dev' or 'prod'."
    exit 1
fi

BUILD_ENV="$1"

# Search for directories ending with "-dev" or "-prod" and change into the corresponding one
TARGET_DIR="_$BUILD_ENV" # _dev, _prod
FULL_TARGET_DIR="$WORKING_DIR/$TARGET_DIR"
if [ -z "$FULL_TARGET_DIR" ]; then
    echo "No directory found for $1"
    exit 1
fi

compose_env_default="$WORKING_DIR/compose_default.env"
source "$compose_env_default"

if [ ! -f "$FULL_TARGET_DIR/$COMPOSE_ENV_FILE" ]; then
    cp $WORKING_DIR/java_template_$BUILD_ENV.env $FULL_TARGET_DIR/$COMPOSE_ENV_FILE
fi

compose_env_project="$FULL_TARGET_DIR/$COMPOSE_ENV_FILE"
source "$compose_env_project"

mkdir -p "$FULL_TARGET_DIR/java"

if [ -f "$WORKING_DIR/../project.env" ]; then
    # if project.env exists, extract the project name
    source "$WORKING_DIR/../project.env"
else
    # if project.env does not exist, extract the project name from the working dir
    PROJECT_NAME=$(basename "$WORKING_DIR")
fi

COMPOSE_PROJECT_NAME="$PROJECT_NAME-$BUILD_ENV"
TARGET_JAR="$FULL_TARGET_DIR/java/app.jar"
STATIC_FULL_DIR="$FULL_TARGET_DIR/java/$STATIC_RES_DIR"


# Read the contents of the first environment file into COMPOSE_ENV_VARS
COMPOSE_ENV_VARS=$(<"$compose_env_default")

# Extract variable names from the second environment file
second_file_vars=$(<"$compose_env_project" awk -F= '{print $1}')

# Filter out duplicate variables from the first file (vars from the second file will override them)
for var in $second_file_vars; do
    COMPOSE_ENV_VARS=$(echo "$COMPOSE_ENV_VARS" | grep -v "^$var=")
done

# Append additional environment variables from the second file on new lines
COMPOSE_ENV_VARS+="
$(<"$compose_env_project")"

COMPOSE_ENV_VARS=$(echo "$COMPOSE_ENV_VARS" | tr '\n' ' ')
