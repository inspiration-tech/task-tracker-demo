#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source $script_dir/_init.sh $1

cd "$FULL_TARGET_DIR"

# check if there are already launched docker containers
if docker ps --format "{{.Names}}" | grep -q "$COMPOSE_PROJECT_NAME-java"; then
    project_running=1
else
    project_running=0
fi

if [ $project_running = 1 ]; then
    echo "There are running containers matching '$COMPOSE_PROJECT_NAME-java', restarting only the java container"
    eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME stop java"
fi

cd -

if [ -n "$2" ]; then
    mv "$2" "$TARGET_JAR"
    chmod +x "$TARGET_JAR"
fi

cd "$FULL_TARGET_DIR"

if [ $project_running = 1 ]; then
    eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME up -d java"
else
    echo "No containers matching '$COMPOSE_PROJECT_NAME-java' are running. Starting all containers"
    eval "$COMPOSE_ENV_VARS docker compose --project-name $COMPOSE_PROJECT_NAME up -d"
fi

cd -
