#!/bin/bash

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source $script_dir/_init.sh $1

cd "$FULL_TARGET_DIR"
eval "$COMPOSE_ENV_VARS docker compose --file $script_dir/$TARGET_DIR/docker-compose_postgres.yml --project-name $COMPOSE_PROJECT_NAME down"
cd -
